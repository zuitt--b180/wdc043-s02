import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] fruitsArray = new String[5];
        fruitsArray[0] = "apple";
        fruitsArray[1] = "avocado";
        fruitsArray[2] = "banana";
        fruitsArray[3] = "kiwi";
        fruitsArray[4] = "orange";
        System.out.println("Fruits in stock:" + Arrays.toString(fruitsArray));

        System.out.println("Which fruit would you like to get the index of?");
        Scanner userInput = new Scanner (System.in);
        String searchFruit = userInput.nextLine();
        int result = Arrays.binarySearch(fruitsArray, searchFruit);
        System.out.println("The index of " + searchFruit + " is: " + result);


        ArrayList<String> friends = new ArrayList<>();
        friends.add("Toby");
        friends.add("Gabby");
        friends.add("Jethro");
        friends.add("Prince");
        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventory = new HashMap<>();
        inventory.put ("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of:");
        System.out.println(inventory);





    }
}
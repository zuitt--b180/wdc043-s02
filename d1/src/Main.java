import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Operators in Java

        //Operators:
        //Arithmetic: +, -, *, /, %
        //Comparison: >,<,>=,<=, ==, !=
        //Logical: &&, ||, !
        //Assignment: =

        //Control Structures in Java
        //if statements allow us to manipulate the flow of the code depending on the evaluation of logical expressions as our condition.

//        if(condition){
//
//        }


        int num1 = 15;
        if (num1 % 5 == 0){
            System.out.println(num1 + " is divisible by 5");
        }

        //else statement will allow us to run a task or code if the condition fails or receives falsy value.

        num1 = 36;
        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        /*
            MINI ACTIVITY

            Create/Instantiate a new Scanner object from the scanner class and name it numberScanner.
            Ask the user for an integer and store the input in a variable.
            Add an if-else statement:
                if the number is even, show a message:
                <number> is even!
                Else, show the following message:
                <number> is odd!

        */

       /* Scanner numberScanner = new Scanner (System.in);

        System.out.println("Please enter a number:");
        int integer = numberScanner.nextInt();

        if (integer % 2 == 0 ){
            System.out.println(integer + " is even.");
        } else {
            System.out.println(integer + " is odd.");
        }*/

        /*
            Switch Cases
            Switch statements are control flow structures that allows one code block to be run out of many code blocks.
            We compare the given value against each cases, and if a case matches we will run that code block.
            This is mostly used if the user input is predictable.
        */

       /* System.out.println("Enter a number from 1-4 to see one of the four directions");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){

            case 1:
                System.out.println("North");
                break; //to break the process of the code block once it finishes.
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Out of Range!");
        }
*/
        //Arrays
            //Arrays are objects that contain a fixed/limited number of values of a single data type.
            //Unlike in other languages like JS, the length of Java arrays are established when the array is created.
            //Syntax
            //dataType[] identifier = new dataType[numberOfElements];
            //dataType [] identifier = {elementA, elementB,...};


        String[] newArray = new String[3];
        newArray[0] = "Clark";
        newArray[1] = "Bruce";
        newArray[2] = "Diana";
        //newArray[1]='a'; - accepts only a string
        //newArray[3] = "Barry"; - the length of a Java array is set upon creation.

        //This will show the memory address of the array or the location of the array within the memory.
        System.out.println(newArray);
        //We use the Arrays class to access methods to manipulate and access our array.
        ///toString() is used to show the values of the array as a string in the terminal.
        System.out.println(Arrays.toString(newArray));

        //Array Methods
            //toString() - retrieve the actual value of the array as a string.
        //Sort Method
        Arrays.sort(newArray);
        System.out.println("Result of Arrays.sort()");
        System.out.println(Arrays.toString(newArray));

        Integer [] intArr = new Integer [3];
        intArr [0] = 54;
        intArr [1] = 12;
        intArr [2] = 67;
        System.out.println("Initial order of the intArr:");
        System.out.println(Arrays.toString(intArr));

        Arrays.sort(intArr);
        System.out.println("Order of the items in intArr after sort():");
        System.out.println(Arrays.toString(intArr));

        //binarySearch() method - allows us to pass an argument/item to search for within our array.  binarySearch() will then return the index number of the found element.
        //You can use a scanner to get input for your search
        String searchTerm = "Bruce";
        int result = Arrays.binarySearch(newArray, searchTerm);
        System.out.println("The index of " + searchTerm + " is: " + result);

        /*
            Array List
            -Array lists are resizable arrays that function similarly to how arrays work in other languages like JS.
            -Using the new keyword in creating an ArrayList does not require the datatype of the array list to be defined to avoid repetition.
            -Syntax:
                ArrayList<dataType> identifier = new ArrayList<>();
        */

        ArrayList<String> students = new ArrayList<>();

        //ArrayList Methods
        //arrayListName.add(<itemToAdd>) - adds elements in our array list.
        students.add ("Paul");
        students.add ("John");
        System.out.println(students);

        //arrayListName.get(index) - retrieve items from our array list using its index.
        System.out.println(students.get(1));

        //arrayListName.set(index, value) - update an item by its index.
        students.set(0, "George");
        System.out.println(students);

        //arrayListName.remove(index) - remove an item from the arrayList based on its index.
        students.remove(1);
        System.out.println(students);
        students.add("James");
        students.add("Wade");
        System.out.println(students);
        students.remove(1);
        System.out.println(students);

        //arrayListName.clear() - clears out items in the array list:
        students.clear();
        System.out.println(students);

        //arrayListName.size() - gets the length of our array list
        System.out.println(students.size());

        //Arrays with initialized values:
        double[] doubleArr = {76.54, 80.02, 85.54, 79.77};
        System.out.println(Arrays.toString(doubleArr));
        //doubleArr[4] = 93.22; //you cannot add more elements
        //still not allowed to go over the initial length of the array.

        //ArrayList with initialized values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("Junie", "Cong"));
        System.out.println(employees);
        employees.add("Red");
        System.out.println(employees);

        //HashMaps
        //Most objects in Java are defines as Classes that contains a proper set of properties and methods.  However, there might be cases where this is not appropriate for your use-case or you may simply want to store a collection of data that in a key-value pair.
        //In Java, "keys" are also referred as "fields".
        //This offers flexibility when storing a collection of data.
        //Syntax
            //HashMap<fieldDataType, valueDataType> identifier = new HashMap<>();

        HashMap<String, String> userRoles = new HashMap<>();
        //Add new fields in the hashmap:
            //hashMapName.put(<item>);
        userRoles.put("Anna", "Admin");
        userRoles.put("Alice", "User");
        System.out.println(userRoles);
        userRoles.put("Alice", "Teacher");
        System.out.println(userRoles);
        userRoles.put("Dennis", "Admin");
        System.out.println(userRoles);

        //retrieve values method by fields
        //hashMapName.get("field");
        System.out.println(userRoles.get("Alice"));
        System.out.println(userRoles.get("Dennis"));
        System.out.println(userRoles.get("alice")); //if field is not present, returns null

        //remove an element
        //hashMapName.remove("field");
        userRoles.remove("Dennis");
        System.out.println(userRoles);

        //retrieve hashMap keys
        //hashMapName.keySet();
        System.out.println(userRoles.keySet());
        System.out.println(userRoles.values());

    }
}